require 'rails_helper'

RSpec.feature "Products", type: :feature do
  describe "GET #show" do
    let!(:product) { create(:product) }

    scenario "visit show" do
      visit potepan_product_path(product.id)
      expect(page).to have_current_path potepan_product_path(product.id)
      expect(page).to have_title "#{product.name} | BIGBAG store"
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).to have_content product.description
    end
  end
end
