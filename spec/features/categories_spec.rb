require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  describe "GET #show" do
    let!(:taxonomy) { create(:taxonomy) }
    let!(:taxon) { create(:taxon, taxonomy: taxonomy, parent: taxonomy.root) }
    let!(:mugs) { create(:taxon, name: "mugs", parent: taxonomy.root) }
    let!(:product) { create(:product, taxons: [taxon]) }

    before do
      visit potepan_category_path(taxon.id)
    end

    scenario "visit category pages" do
      within "div.main-wrapper" do
        expect(page).to have_current_path potepan_category_path(taxon.id)
        expect(page).to have_title "#{taxon.name} | BIGBAG store"
      end
      within "div.sideBar" do
        expect(page).to have_content taxonomy.name
        expect(page).to have_content taxon.name
      end
      within "div.productBox" do
        expect(page).to have_content product.name
        expect(page).to have_content product.display_price
      end
    end

    scenario "should be able to visit products" do
      expect(page).to have_link product.name, href: potepan_product_path(product.id)
    end

    scenario "transition to the correct page" do
      click_link mugs.name
      expect(current_path).to eq potepan_category_path(mugs.id)
    end
  end
end
