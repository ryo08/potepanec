require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "GET #show" do
    let!(:product) { create(:product) }

    before do
      get :show, params: { id: product.id }
    end

    it "responds successfully" do
      expect(response).to be_successful
    end

    it "returns a 200 ok response" do
      expect(response).to have_http_status(:ok)
    end

    it "render_template :show" do
      expect(response).to render_template(:show)
    end

    it "assigns @product" do
      expect(assigns(:product)).to eq(product)
    end
  end
end
