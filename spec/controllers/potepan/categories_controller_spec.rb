require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "GET #show" do
    let!(:taxon)    { create(:taxon) }
    let!(:taxonomy) { create(:taxonomy) }
    let!(:product1) { create(:product, taxons: [taxon]) }
    let!(:product2) { create(:product) }

    before do
      get :show, params: { id: taxon.id }
    end

    it "responds successfully" do
      expect(response).to be_successful
    end

    it "returns a 200 ok response" do
      expect(response).to have_http_status(:ok)
    end

    it "render_template :show" do
      expect(response).to render_template(:show)
    end

    it "assigns @taxon" do
      expect(assigns(:taxon)).to eq(taxon)
    end

    it "assigns @taxonomies" do
      expect(assigns(:taxonomies)).to include(taxonomy)
    end

    it "assigns @product" do
      expect(assigns(:products)).to contain_exactly(product1)
    end

    it "not assigns @product" do
      expect(assigns(:products)).not_to contain_exactly(product2)
    end
  end
end
